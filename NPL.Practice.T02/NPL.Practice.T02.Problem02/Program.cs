﻿using System;

namespace NPL.Practice.T02.Problem02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            try
            {
                Console.WriteLine(FindMaxSubArray(new[] { 1, 2, 4, 5, 6 }, 3));
                Console.WriteLine(FindMaxSubArray(new[] { 1, 14, 4, 5, 6 }, 1));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            Console.ReadKey();
        }

        /// <summary>
        ///  Find the contiguous subarray
        ///  with length equal subLength that has the maximum sum.
        /// </summary>
        /// <param name="inputArray"> an array of integers </param>
        /// <param name="subLength"> length of contiguous subarray</param>
        /// <returns> the maximum contiguous subarray sum: int</returns>
        /// Exception: throw exception when Length of inputArray less than subLength, subLength <= 0
        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            var maxNumber = Constant.MAX_NUMBER;
            // Check input is valid
            ValidateInput(inputArray, subLength);

            for (int i = 0; i <= inputArray.Length - subLength; i++)
            {
                var sumOfSubArray = 0;
                //Calc sum of sub array
                for (int j = i; j < i + subLength; j++)
                {
                    sumOfSubArray += inputArray[j];
                }

                if (maxNumber < sumOfSubArray)
                {
                    maxNumber = sumOfSubArray;
                }
            }
            return maxNumber;
        }

        /// <summary>
        /// Check input is valid, if invalid then throw exception
        /// </summary>
        /// <param name="inputArray"></param>
        /// <param name="subLength"></param>
        static private void ValidateInput(int[] inputArray, int subLength)
        {
            if (inputArray.Length < subLength)
            {
                throw new ArgumentException("Length of inputArray must be greater than subLength");
            }
            
            if (subLength <= 0)
            {
                throw new ArgumentException("subLength must be greater than 0");
            }
        }
    }
}
