﻿using System;

namespace NPL.Practice.T02.Problem01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("NPL.Practice.T02.Problem01");
            try
            {
                Console.WriteLine(GetArticleSummary("One of the world's biggest festivals hit the streets of London", 50));
                Console.WriteLine(GetArticleSummary("One of the world's biggest festivals hit the streets of London", 3));
                Console.WriteLine(GetArticleSummary("One of the world's biggest festivals hit the streets of London", 1));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            
            Console.ReadKey();
        }
        /// <summary>
        /// Summary of an article
        /// </summary>
        /// <param name="content">content of article: string</param>
        /// <param name="maxLength">max length of summary: int</param>
        /// <returns>summary of the article: string</returns>
        static public string GetArticleSummary(string content, int maxLength)
        {
            //Check valid input
            ValidateInput(maxLength);

            if (content.Length <= maxLength)
            {
                return content;
            }

            var substring = content.Substring(0, maxLength);
            var lastSpaceIndex = 0;
            //Reverse loop for find last space in string
            for (int i = substring.Length - 1; i >= 0; i--)
            {
                if (substring[i].Equals(' '))
                {
                    lastSpaceIndex = i;
                    break;
                }
            }

            //If lastSpaceIndex = 0 mean string has 1 word then return "..."
            if (lastSpaceIndex == 0)
            {
                return Constant.THREE_DOTS;
            }

            substring = substring.Substring(0, lastSpaceIndex);
            var result = substring + Constant.THREE_DOTS;

            return result;
        }

        /// <summary>
        /// Check input is valid, if invalid then throw exception
        /// </summary>
        /// <param name="maxLength"></param>
        static private void ValidateInput(int maxLength)
        {
            if (maxLength <= 0)
            {
                throw new ArgumentException("maxLength must be greater than 0");
            }
        }
    }
}
