﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPL.Practice.T02.Problem03
{
    public enum GraduateLevel
    {
        Excellent, 
        Very_Good, 
        Good, 
        Average, 
        Failed
    }
}
