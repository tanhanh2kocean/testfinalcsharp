﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPL.Practice.T02.Problem03
{
    public static class StudentExtension
    {
        public static string GetCertificate(this Student student)
        {
            return $"Name: {student.Name}, SqlMark: {student.SqlMark}, CsharpMark: {student.CsharpMark}, " +
                $"DsaMark: {student.DsaMark}, GPA: {student.GPA}, GraduateLevel: {student.GraduateLevel}.";
        }
    }
}