﻿using System;
using System.Collections.Generic;

namespace NPL.Practice.T02.Problem03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("NPL.Practice.T02.Problem03");
            var students = new List<Student>();
            students.Add(new Student(1, "Bui Tan Hanh", DateTime.Now, 8, 9.5m, 9));
            students.Add(new Student(2, "Luong Son Ba", DateTime.Now, 9.5m, 9.5m, 10));
            students.Add(new Student(3, "Chuc Anh Dai", DateTime.Now, 7m, 7.5m, 7));
            foreach (var student in students)
            {
                student.Graduate();
                Console.WriteLine(student.GetCertificate());
                Console.WriteLine("---------------------------------------------");
            }
            
            Console.ReadKey();
        }
    }
}
