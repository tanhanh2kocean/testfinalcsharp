﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPL.Practice.T02.Problem03
{
    public class Student : IGraduate
    {
        public Student(int id, string name, DateTime startDate, decimal sqlMark,
            decimal csharpMark, decimal dsaMark)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            SqlMark = sqlMark;
            CsharpMark = csharpMark;
            DsaMark = dsaMark;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public void Graduate()
        {
            var gpa = calcGPA(SqlMark, CsharpMark, DsaMark);
            var graduateLevel = convertGpaToGraduateLevel(gpa);
            GPA = gpa;
            GraduateLevel = graduateLevel;
        }

        /// <summary>
        /// Function calculate GPA of student
        /// </summary>
        /// <param name="sqlMark"></param>
        /// <param name="csharpMark"></param>
        /// <param name="dsaMark"></param>
        /// <returns> GPA of student: demical </returns>
        private decimal calcGPA(decimal sqlMark, decimal csharpMark, decimal dsaMark)
        {
            return (sqlMark + csharpMark + dsaMark) / 3;
        }

        /// <summary>
        /// Convert Gpa To GraduateLevel
        /// </summary>
        /// <param name="gpa"></param>
        /// <returns> GraduateLevel match GPA: GraduateLevel</returns>
        private GraduateLevel convertGpaToGraduateLevel(decimal gpa)
        {
            if (0 <= gpa && gpa < 5)
            {
                return GraduateLevel.Failed;
            }

            if (5 <= gpa && gpa < 7)
            {
                return GraduateLevel.Average;
            }
            if (7 <= gpa && gpa < 8)
            {
                return GraduateLevel.Good;
            }
            if (8 <= gpa && gpa < 9)
            {
                return GraduateLevel.Very_Good;
            }

            return GraduateLevel.Excellent;
        }

        /// <summary>
        /// Return a string with information: Name , SqlMark, CsharpMark,
        /// DsaMark, GPA, GraduateLevel
        /// </summary>
        /// <returns></returns>
        public string GetCertificate()
        {
            return $"Name: {Name}, SqlMark: {SqlMark.ToString("F2")}, CsharpMark: {CsharpMark.ToString("F2")}, " +
                $"DsaMark: {DsaMark.ToString("F2")}, GPA: {GPA.ToString("F2")}, GraduateLevel: {GraduateLevel}.";
        }
    }
}
